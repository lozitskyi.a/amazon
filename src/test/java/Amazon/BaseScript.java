package Amazon;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public abstract class BaseScript {

    public static final String DEFAULT_BASE_URL = "https://www.amazon.in/";

    //configuration
    public static void configuration() throws Exception {
        Configuration.browser = "chrome";
        Configuration.browserSize = "1600x900";
        Configuration.timeout = 10000;
        Configuration.headless = true;
//        Configuration.startMaximized = true;
        System.setProperty("chromeoptions.args", "--disable-gpu");
        System.setProperty("chromeoptions.args", "--no-sandbox");
        System.setProperty("chromeoptions.args", "--disable-dev-shm-usage");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setAcceptInsecureCerts(true);
        Configuration.browserCapabilities = capabilities;
//        clearBrowserCookies();
    }

}