package Amazon.tests;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static Amazon.BaseScript.DEFAULT_BASE_URL;
import static Amazon.BaseScript.configuration;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class Add_item_to_Cart {
    @Before
    public void setUp() throws Exception {
        configuration();
    }
    @Test
    public void main() throws InterruptedException {
        //open https://www.amazon.in/
        open(DEFAULT_BASE_URL);
        //enter 'samsung s8' in search field
        $("#twotabsearchtextbox").sendKeys("samsung s8");
        //click on search button
        $("input.nav-input").click();
        //choose 'Samsung' in 'Brand'
        $(By.xpath("//span[text()='Samsung']")).click();
        //choose '64GB' in 'Internal Memory'
        $(By.xpath("//span[text()='64 GB']")).click();
        //click on first 'Samsung Galaxy S8' in search result
        $("div.s-result-list.sg-row > div:nth-child(1) > div > div > div > div:nth-child(2) > div > div > div > span > a").click();
        //switch to active tab
        switchTo().window(1);
        //get product title
        String title = $("#productTitle").getText();
        //click on 'Add to Cart' button
        $("#add-to-cart-button").click();
        //click on 'Cart' button
        $("#hlb-view-cart-announce").click();
        //check that product are in Cart
        $(".sc-product-title").shouldHave(text(title));
    }
}
